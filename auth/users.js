const userauth = {
  APP_USER_AUTH: process.env.APP_USER_AUTH,
  APP_PWD_AUTH: process.env.APP_PWD_AUTH,
};

module.exports = userauth;