var express = require("express");
var app = express();
var users = require("./auth/users.js");
var jwt = require("jwt-simple");
var jwtoken = require('jsonwebtoken');
var cfg = require("./config/config.js");

var decodeToken = function (token){
  var json = jwtoken.verify(token, cfg.jwtSecret);
  return true;  
}

var getToken = function (username, password, secretkey) {
  var payload = {name: username, password: password};
  var token = jwt.encode(payload, secretkey);
  return token;
}    

module.exports.getToken = getToken;
module.exports.decodeToken = decodeToken;