#!/usr/bin/env node

var cmd = require('commander')
var authenticate = require('../lib.js')

cmd.version('1.0.0')

cmd.command('generate <username> <password> <secretkey>')
    .description('Realiza uma tokenização dos dados necessários para a aplicação')
    //.option('-t, --throws', 'OpÃ§Ã£o que ativa a quebra de execuÃ§Ã£o via exceÃ§Ã£o')
    .action( function (username, password, secretkey){
      let token = authenticate.getToken(username, password, secretkey)
      console.info(token);
      process.exitCode = 1;
})

cmd.parse(process.argv)

